package wallas.swing.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.plaf.UIResource;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import wallas.swing.combobox.JComboboxRefined;
import wallas.util.event.selection.SelectionEventFire;
import wallas.util.event.selection.SelectionListener;
import wallas.util.form.rfb.Cnpj;
import wallas.util.form.rfb.Cpf;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <V>
 */
public class JTableRefined<V> extends JTable {

    private static final Color COLOR_BLUE = new Color(0, 150, 255);
    private static final Color COLOR_LIGHTBLUE = new Color(230, 245, 255);
    private SelectionEventFire selectionEvent;
    private JPopupMenu jPopupMenu;
    private int selectedRowIndex = -1;
    private long lastTime;
    private final long timeFactor = 130;

    public JTableRefined() {
        initComponents();
    }

    private void initComponents() {
        selectionEvent = new SelectionEventFire();
        jPopupMenu = new JPopupMenu();

        ListSelectionModel listSelection = getSelectionModel();
        listSelection.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listSelection.addListSelectionListener(this::jTableValueChanged);

        setShowGrid(false);
        setIntercellSpacing(new Dimension(0, 0));
        addMouseListener(showPopupMenuMouseClicked());
    }

    public void setModelComponent(JTableModel<V> jTableModel) {
        setModel(jTableModel);
    }

    public JTableModel<V> getModelComponent() {
        if (getModel() instanceof JTableModel) {
            return (JTableModel) getModel();
        } else {
            return null;
        }
    }

    private void jTableValueChanged(ListSelectionEvent evt) {
        if (!evt.getValueIsAdjusting()) {
            int row = getSelectedRow();
            if (row >= 0) {
                if (timeFactor()) {
                    selectionEvent.fireValueSelected(row);
                    selectedRowIndex = row;
                }
            } else {
                selectionEvent.fireNoneSelected(-1);
            }
        }
        scrollRectToVisible(getCellRect(getSelectedRow(), 0, true));
    }

    private boolean timeFactor() {
        boolean flagTime;
        long currentTime = System.currentTimeMillis();
        flagTime = currentTime - lastTime > timeFactor;
        lastTime = currentTime;
        return flagTime;
    }

    private MouseAdapter showPopupMenuMouseClicked() {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getButton() == MouseEvent.BUTTON3) {
                    int row = rowAtPoint(evt.getPoint());
                    if (row != -1) {
                        if (jPopupMenu.getComponentCount() != 0) {
                            jPopupMenu.show(JTableRefined.this, evt.getX(), evt.getY());
                        }
                        setRowSelectionInterval(row, row);
                    }
                }
            }
        };
    }

    public void addMenuItem(JMenuItem jMenuItem) {
        jPopupMenu.add(jMenuItem);
    }

    public boolean isEmptyTable() {
        return (getRowCount() == 0);
    }

    public void setRowSelectionFirst() {
        setRowSelectionInterval(0, 0);
    }

    public void setRowSelectionLast() {
        setRowSelectionInterval(0, (getRowCount() - 1));
    }

    public void setRowSelectionPrevious() {
        int counter = (getSelectedRow() - 1);
        setRowSelectionInterval(0, ((counter >= 0) ? counter : (getRowCount() - 1)));
    }

    public void setRowSelectionNext() {
        int counter = (getSelectedRow() + 1);
        setRowSelectionInterval(0, ((counter < getRowCount()) ? counter : 0));
    }

    public void setRowSelectionCurrent() {
        if (selectedRowIndex >= 0) {
            if (selectedRowIndex < getRowCount()) {
                setRowSelectionInterval(0, selectedRowIndex);
            } else {
                setRowSelectionInterval(0, selectedRowIndex - 1);
            }
        }
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Object value = getValueAt(row, column);

        boolean isSelected = false;
        boolean hasFocus = false;

        if (!isPaintingForPrint()) {
            isSelected = isCellSelected(row, column);

            boolean rowIsLead = (selectionModel.getLeadSelectionIndex() == row);
            boolean colIsLead = (columnModel.getSelectionModel().getLeadSelectionIndex() == column);

            hasFocus = (rowIsLead && colIsLead) && isFocusOwner();
        }

        Component c = renderer.getTableCellRendererComponent(this, value, isSelected, hasFocus, row, column);

        Component d = (c instanceof JComboboxRefined) ? ((JComboboxRefined) c).getEditorComponent() : c;

        if (isSelected) {
            d.setForeground(Color.WHITE);
            d.setBackground(COLOR_BLUE);
        } else {
            d.setForeground(Color.BLACK);
            d.setBackground((row % 2 == 0) ? Color.WHITE : COLOR_LIGHTBLUE);
        }

        return c;
    }

    public void addSelectionListener(SelectionListener listener) {
        selectionEvent.addListener(listener);
    }

    public void removeSelectionListener(SelectionListener listener) {
        selectionEvent.removeListener(listener);
    }

    @Override
    protected void createDefaultRenderers() {
        defaultRenderersByColumnClass = new UIDefaults(8, 0.75f);

        defaultRenderersByColumnClass.put(Object.class, (UIDefaults.LazyValue) t -> new DefaultTableCellRenderer.UIResource());

        defaultRenderersByColumnClass.put(Number.class, (UIDefaults.LazyValue) t -> new NumberRenderer());

        defaultRenderersByColumnClass.put(Float.class, (UIDefaults.LazyValue) t -> new DoubleRenderer());
        defaultRenderersByColumnClass.put(Double.class, (UIDefaults.LazyValue) t -> new DoubleRenderer());
        defaultRenderersByColumnClass.put(BigDecimal.class, (UIDefaults.LazyValue) t -> new DoubleRenderer());

        defaultRenderersByColumnClass.put(Date.class, (UIDefaults.LazyValue) t -> new DateRenderer());

        defaultRenderersByColumnClass.put(Icon.class, (UIDefaults.LazyValue) t -> new IconRenderer());
        defaultRenderersByColumnClass.put(ImageIcon.class, (UIDefaults.LazyValue) t -> new IconRenderer());

        defaultRenderersByColumnClass.put(Boolean.class, (UIDefaults.LazyValue) t -> new BooleanRenderer());

        defaultRenderersByColumnClass.put(Cpf.class, (UIDefaults.LazyValue) t -> new CpfRenderer());
        defaultRenderersByColumnClass.put(Cnpj.class, (UIDefaults.LazyValue) t -> new CnpjRenderer());
    }

    static class NumberRenderer extends DefaultTableCellRenderer.UIResource {

        public NumberRenderer() {
            super();
            setHorizontalAlignment(JLabel.CENTER);
        }
    }

    static class DoubleRenderer extends DefaultTableCellRenderer.UIResource {

        NumberFormat formatter;

        public DoubleRenderer() {
            super();
            setHorizontalAlignment(JLabel.RIGHT);
        }

        @Override
        public void setValue(Object value) {

            if (formatter == null) {
                formatter = NumberFormat.getInstance();
                formatter.setMinimumFractionDigits(2);
            }
            setText((value == null) ? "" : formatter.format(value));
        }
    }

    static class DateRenderer extends DefaultTableCellRenderer.UIResource {

        DateFormat formatter;

        public DateRenderer() {
            super();
        }

        @Override
        public void setValue(Object value) {
            if (formatter == null) {
                formatter = DateFormat.getDateInstance();
            }
            setText((value == null) ? "" : formatter.format(value));
        }
    }

    static class IconRenderer extends DefaultTableCellRenderer.UIResource {

        public IconRenderer() {
            super();
            setHorizontalAlignment(JLabel.CENTER);
        }

        @Override
        public void setValue(Object value) {
            setIcon((value instanceof Icon) ? (Icon) value : null);
        }
    }

    static class BooleanRenderer extends JCheckBox implements TableCellRenderer, UIResource {

        private static final Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

        public BooleanRenderer() {
            super();
            setHorizontalAlignment(JLabel.CENTER);
            setBorderPainted(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            setSelected((value != null && ((Boolean) value)));

            if (hasFocus) {
                setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            } else {
                setBorder(noFocusBorder);
            }

            return this;
        }
    }

    static class CpfRenderer extends DefaultTableCellRenderer.UIResource {

        public CpfRenderer() {
            super();
        }

        @Override
        public void setValue(Object value) {
            setText((value == null) ? "" : ((Cpf) value).format());
        }
    }

    static class CnpjRenderer extends DefaultTableCellRenderer.UIResource {

        public CnpjRenderer() {
            super();
        }

        @Override
        public void setValue(Object value) {
            setText((value == null) ? "" : ((Cnpj) value).format());
        }
    }

}
