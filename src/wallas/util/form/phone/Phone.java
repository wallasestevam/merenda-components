package wallas.util.form.phone;

import java.text.ParseException;
import java.util.Objects;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Phone {

    private String phone;

    public Phone() {
    }

    public String format() {
        try {
            String PHONE = "(##)####-####";
            MaskFormatter mf = new MaskFormatter(PHONE);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(phone);
        } catch (ParseException ex) {
            return "";
        }
    }

    public Phone parse(String phone) throws ParseException {
        final int LENGTH = 10;
        if ((phone != null) && (!phone.isEmpty())) {
            phone = phone.replaceAll("[^0-9]", "");
            if (phone.length() == LENGTH) {
                this.phone = phone;
                return this;
            } else {
                throw new ParseException(phone, 0);
            }
        } else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.phone);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Phone other = (Phone) obj;

        return Objects.equals(this.phone, other.phone);
    }

    @Override
    public String toString() {
        return String.valueOf(phone);
    }

}
