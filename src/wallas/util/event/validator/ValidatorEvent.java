package wallas.util.event.validator;

/**
 *
 * @author José Wallas Clemente Estemva
 */
public interface ValidatorEvent {

    public Object getActionObject();

}
