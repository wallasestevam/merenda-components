package wallas.util.event.validator;

import java.util.ArrayList;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class ValidatorEventFire {

    protected final ArrayList<ValidatorActionListener> listeners;

    public ValidatorEventFire() {
        listeners = new ArrayList<>();
    }

    public void addListener(ValidatorActionListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(ValidatorActionListener listener) {
        listeners.remove(listener);
    }

    public void removeListeners() {
        listeners.clear();
    }

    public void fireEventPassed(Object object) {
        ValidatorEvent x = new ValidatorEvent() {
            @Override
            public Object getActionObject() {
                return object;
            }
        };
        for (ValidatorActionListener listener : listeners) {
            listener.passed(x);
        }
    }

    public void fireEventFailed(Object object) {
        ValidatorEvent x = new ValidatorEvent() {
            @Override
            public Object getActionObject() {
                return object;
            }
        };
        for (ValidatorActionListener listener : listeners) {
            listener.failed(x);
        }
    }

    public void fireEventEmpty(Object object) {
        ValidatorEvent x = new ValidatorEvent() {
            @Override
            public Object getActionObject() {
                return object;
            }
        };
        for (ValidatorActionListener listener : listeners) {
            listener.empty(x);
        }
    }

}
