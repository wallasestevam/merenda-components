package wallas.util.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class RuntimeProcess {

    private StringBuffer successMessage;
    private StringBuffer erroMessage;
    private boolean errorRunning;

    public RuntimeProcess() {
    }

    public void execute(String command) {
        successMessage = new StringBuffer();
        erroMessage = new StringBuffer();

        try {
            Process process = Runtime.getRuntime().exec(command);

            InputStream successStream = process.getInputStream();
            InputStream erroStream = process.getErrorStream();

            BufferedReader exitProcess;
            String string;

            exitProcess = new BufferedReader(new InputStreamReader(successStream));
            while ((string = exitProcess.readLine()) != null) {
                successMessage.append(string).append("\r\n");
            }

            exitProcess = new BufferedReader(new InputStreamReader(erroStream));
            while ((string = exitProcess.readLine()) != null) {
                errorRunning = true;
                erroMessage.append(string).append("\r\n");
            }

            process.waitFor();

            exitProcess.close();
            erroStream.close();
            successStream.close();
            process.getOutputStream().close();

        } catch (IOException | InterruptedException ex) {
            errorRunning = true;
            erroMessage.append(ex.toString());
        }
    }

    public String getSucessMessage() {
        if (successMessage.length() > 0) {
            return successMessage.toString();
        } else {
            return null;
        }
    }

    public String getErroMessage() {
        if (erroMessage.length() > 0) {
            return erroMessage.toString();
        } else {
            return null;
        }
    }

    public boolean isErrorRunning() {
        return errorRunning;
    }

}
