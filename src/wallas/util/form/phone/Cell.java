package wallas.util.form.phone;

import java.text.ParseException;
import java.util.Objects;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Cell {

    private String cell;

    public Cell() {
    }

    public String format() {
        try {
            String CELL = "(##)#####-####";
            MaskFormatter mf = new MaskFormatter(CELL);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(cell);
        } catch (ParseException ex) {
            return "";
        }
    }

    public Cell parse(String cell) throws ParseException {
        final int LENGTH = 11;
        if ((cell != null) && (!cell.isEmpty())) {
            cell = cell.replaceAll("[^0-9]", "");
            if (cell.length() == LENGTH) {
                this.cell = cell;
                return this;
            } else {
                throw new ParseException(cell, 0);
            }
        } else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.cell);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Cell other = (Cell) obj;

        return Objects.equals(this.cell, other.cell);
    }

    @Override
    public String toString() {
        return String.valueOf(cell);
    }

}
