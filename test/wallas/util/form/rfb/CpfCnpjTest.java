package wallas.util.form.rfb;

import java.text.ParseException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CpfCnpjTest {

    @Test
    public void CpfTest() throws ParseException {
        Cpf cpf = new Cpf();
        cpf.parse("06833518424");
        assertEquals("068.335.184-24", cpf.format());
    }

    @Test
    public void CnpjTest() throws ParseException {
        Cnpj cnpj = new Cnpj();
        cnpj.parse("10146371000130");
        assertEquals("10.146.371.0001-30", cnpj.format());
    }

}
