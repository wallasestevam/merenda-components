package wallas.util.connection.ftp;

import java.io.Serializable;
import java.util.Objects;
import wallas.util.crypt.Crypt;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class FTPConfig implements Serializable {

    private String host;
    private String user;
    private String password;

    public FTPConfig() {
    }

    public FTPConfig(String host, String user, String password) {
        this.host = host;
        this.user = user;
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.host)
                + Objects.hashCode(this.user)
                + Objects.hashCode(this.password);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final FTPConfig other = (FTPConfig) obj;

        return (Objects.equals(this.host, other.host))
                && (Objects.equals(this.user, other.user))
                && (Objects.equals(this.password, other.password));
    }

    private void writeObject(java.io.ObjectOutputStream stream) throws java.io.IOException {
        final Crypt crypt = new Crypt();

        setHost(crypt.encrypt(getHost()));
        setUser(crypt.encrypt(getUser()));
        setPassword(crypt.encrypt(getPassword()));

        stream.defaultWriteObject();
    }

    private void readObject(java.io.ObjectInputStream stream) throws java.io.IOException, ClassNotFoundException {
        final Crypt crypt = new Crypt();

        stream.defaultReadObject();

        setHost(crypt.decrypt(getHost()));
        setUser(crypt.decrypt(getUser()));
        setPassword(crypt.decrypt(getPassword()));
    }

}
