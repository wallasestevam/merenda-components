package wallas.swing.panel.browser;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserNavigationEvent;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * * @author José Wallas Clemente Estevam
 */
public class JPanelBrowser extends JComponent {

    private JWebBrowser jWebBrowser;
    private Timer timer;
    private boolean erro;

    public JPanelBrowser() {
        this(null);
    }

    public JPanelBrowser(String url) {
        initBrowser();
        initLayout();
        initTimer();
        navigate(url);
    }

    private void initBrowser() {
        if (!NativeInterface.isOpen()) {
            NativeInterface.open();
            new Thread(NativeInterface::runEventPump).start();
        }

        jWebBrowser = new JWebBrowser();
        jWebBrowser.setBarsVisible(false);
        jWebBrowser.getNativeComponent().setFocusable(false);
        jWebBrowser.addWebBrowserListener(webBrowserListener());
    }

    private void initLayout() {
        setLayout(new CardLayout());
        add(new JPanel(), "Panel");
        add(jWebBrowser, "Browser");
    }

    private void initTimer() {
        timer = new Timer(60000, this::timerActionPerformed);
        timer.setRepeats(false);
    }

    private WebBrowserAdapter webBrowserListener() {
        return new WebBrowserAdapter() {
            @Override
            public void statusChanged(WebBrowserEvent wbe) {
                if (wbe.getWebBrowser().getStatusText().contains("res://ieframe.dll/dnserrordiagoff.htm")) {
                    timer.start();
                    erro = true;
                }
            }

            @Override
            public void locationChanged(WebBrowserNavigationEvent wbne) {
                if (!erro) {
                    CardLayout cardLayout = (CardLayout) getLayout();
                    cardLayout.last(JPanelBrowser.this);
                }
            }
        };
    }

    private void timerActionPerformed(ActionEvent evt) {
        erro = false;
        reloadPage();
    }

    public void reloadPage() {
        jWebBrowser.navigate(jWebBrowser.getResourceLocation());
    }

    public void navigate(String url) {
        if (url != null) {
            jWebBrowser.navigate(url);
        }
    }

}
