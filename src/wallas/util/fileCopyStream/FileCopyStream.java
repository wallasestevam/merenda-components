package wallas.util.fileCopyStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import wallas.util.event.progress.ProgressEventFire;
import wallas.util.event.progress.ProgressListener;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class FileCopyStream {

    private final ProgressEventFire progressEvent;

    public FileCopyStream() {
        progressEvent = new ProgressEventFire();
    }

    public void copyStream(InputStream is, OutputStream fos, String fileName, long fileSize) throws IOException {
        float totalDataRead = 0;
        byte[] buffer = new byte[1024];
        int len;

        progressEvent.fireNameProgressFile(fileName);

        while ((len = is.read(buffer)) != -1) {
            totalDataRead = totalDataRead + len;
            float percent = (totalDataRead * 100) / fileSize;

            progressEvent.fireProgressStatus((int) percent);

            fos.write(buffer, 0, len);
        }

        fos.close();
        is.close();
    }

    public void addProgressListener(ProgressListener listener) {
        progressEvent.addListener(listener);
    }

    public void removeProgressListener(ProgressListener listener) {
        progressEvent.removeListener(listener);
    }

}
