package wallas.swing.textField.format;

import java.text.ParseException;
import javax.swing.text.Document;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.JTextFieldRefined.Format;
import wallas.util.form.rfb.Cnpj;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CnpjFormatSee extends JTextFieldRefined.FormatSee {

    private static final String MASK = "  .   .   .    -  ";
    private JTextFieldRefined jTextFieldRefined;

    public CnpjFormatSee() {
    }

    @Override
    public void install(JTextFieldRefined jTextFieldRefined) {
        this.jTextFieldRefined = jTextFieldRefined;
        super.install(this.jTextFieldRefined);
    }

    @Override
    public Document getDocument() {
        return new JTextFieldRefined.MaskTextField(MASK, jTextFieldRefined);
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new Format() {
            @Override
            public Object stringToValue(String text) {
                return parse(text);
            }

            @Override
            public String valueToString(Object value) {
                Cnpj cnpj;
                if (value instanceof Cnpj) {
                    cnpj = (Cnpj) value;
                    return cnpj.format();
                } else if (value instanceof String) {
                    cnpj = parse((String) value);
                    return (cnpj != null) ? cnpj.format() : null;
                } else {
                    return null;
                }
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (text == null || text.equals(MASK)) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                if (parse(text) != null) {
                    setMessage("CNPJ Válido!");
                    return true;
                } else {
                    setMessage("CNPJ Inválido!");
                    return false;
                }
            }
        };
    }

    private Cnpj parse(String text) {
        try {
            Cnpj cnpj = new Cnpj();
            cnpj.parse(text);
            return cnpj;
        } catch (ParseException ex) {
            return null;
        }
    }

}
