package wallas.util.form.cep;

import java.text.ParseException;
import java.util.Objects;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Cep {

    private String cep;

    public Cep() {
    }

    public String format() {
        try {
            String CEP = "##.###-###";
            MaskFormatter mf = new MaskFormatter(CEP);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(cep);
        } catch (ParseException ex) {
            return "";
        }
    }

    public Cep parse(String cep) throws ParseException {
        final int LENGTH = 8;
        if ((cep != null) && (!cep.isEmpty())) {
            cep = cep.replaceAll("[^0-9]", "");
            if (cep.length() == LENGTH) {
                this.cep = cep;
                return this;
            } else {
                throw new ParseException(cep, 0);
            }
        } else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.cep);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Cep other = (Cep) obj;

        return Objects.equals(this.cep, other.cep);
    }

    @Override
    public String toString() {
        return String.valueOf(cep);
    }

}
