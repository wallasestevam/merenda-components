package wallas.swing.textField;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import wallas.util.event.GenericActionListener;
import wallas.util.event.GenericEventFire;

/**
 *
 * @author José Wallas Coemente Estevam
 */
public class JTextFieldSearch extends JPanel {

    private JTextField jTextField;
    private JLabel jLabel;
    private Timer timer;
    private GenericEventFire genericEvent;
    private final String SEARCH = "Pesquisar";
    private final String WITHOUT_TEXT = "";
    private String oldText = WITHOUT_TEXT;
    private boolean mouseListener;

    public JTextFieldSearch() {
        initComponents();
    }

    private void initComponents() {
        jTextField = new JTextField();
        jLabel = new JLabel();
        timer = new Timer(800, actionPerformedTimer());
        genericEvent = new GenericEventFire();

        jTextField.setBorder(null);
        jTextField.setDisabledTextColor(Color.LIGHT_GRAY);
        jTextField.addFocusListener(jTextFieldFocusListener());
        jTextField.getDocument().addDocumentListener(jTextFieldDocumentListener());

        setEnabledMouseListener(true);

        timer.setRepeats(false);

        setBackground(jTextField.getBackground());
        setBorder(BorderFactory.createEtchedBorder());
        defaultComponents();
        layoutComponent();
    }

    private void layoutComponent() {
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jTextField)
                .addComponent(jLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }

    private void defaultComponents() {
        jTextField.setForeground(new Color(140, 140, 140));
        jTextField.setText(SEARCH);
        jLabel.setIcon(new ImageIcon(getClass().getResource("/wallas/swing/icon/lupa.png")));
    }

    private void defaultComponentsSeach() {
        jTextField.setForeground(Color.BLACK);
        jTextField.setText(WITHOUT_TEXT);
        jLabel.setIcon(new ImageIcon(getClass().getResource("/wallas/swing/icon/fechar.png")));
    }

    private void startTimeDelay() {
        if (timer.isRunning()) {
            timer.restart();
        } else {
            timer.start();
        }
    }

    private void stopTimeDelay() {
        if (timer.isRunning()) {
            timer.stop();
        }
    }

    private boolean isSearch() {
        return !getText().equals(SEARCH);
    }

    private boolean isBlank() {
        return getText().equals(WITHOUT_TEXT);
    }

    public String getText() {
        return jTextField.getText();
    }

    public void setText(String text) {
        jTextField.setText(text);
    }

    public int getTimer() {
        return timer.getInitialDelay();
    }

    public void setTimer(int timerDalay) {
        timer.setInitialDelay(timerDalay);
    }

    @Override
    public void setEnabled(boolean x) {
        jTextField.setEnabled(x);
        setEnabledMouseListener(x);
        setBackground(jTextField.getBackground());
        super.setEnabled(x);
    }

    private void setEnabledMouseListener(boolean x) {
        if (x != mouseListener) {
            if (x) {
                jLabel.addMouseListener(jLabelMouseClicked());
            } else {
                jLabel.removeMouseListener(jLabel.getMouseListeners()[0]);
            }
            mouseListener = x;
        }
    }

    public void addActionListener(GenericActionListener ouvinte) {
        genericEvent.addListener(ouvinte);
    }

    public void removeActionListener(GenericActionListener ouvinte) {
        genericEvent.removeListener(ouvinte);
    }

    private void fireActionPerformed() {
        if (!oldText.equals(getText())) {
            genericEvent.fireEvent(getText());
            oldText = getText();
        }
    }

    private ActionListener actionPerformedTimer() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTextField.isFocusOwner()) {
                    fireActionPerformed();
                }
            }
        };
    }

    private FocusListener jTextFieldFocusListener() {
        return new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if (!isSearch()) {
                    defaultComponentsSeach();
                    stopTimeDelay();
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (isBlank()) {
                    fireActionPerformed();
                    defaultComponents();
                    stopTimeDelay();
                }
            }
        };
    }

    private DocumentListener jTextFieldDocumentListener() {
        return new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                startTimeDelay();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                startTimeDelay();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                startTimeDelay();
            }
        };
    }

    private MouseAdapter jLabelMouseClicked() {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                jTextField.setText(WITHOUT_TEXT);
                if (jTextField.isFocusOwner()) {
                    jTextField.transferFocus();
                } else {
                    fireActionPerformed();
                    defaultComponents();
                    stopTimeDelay();
                }
            }
        };
    }

}
