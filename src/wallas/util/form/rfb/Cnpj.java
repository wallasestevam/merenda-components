package wallas.util.form.rfb;

import java.text.ParseException;
import java.util.Objects;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Cnpj extends CpfCnpj {

    private String cnpj;

    public Cnpj() {
    }

    public String format() {
        try {
            String CNPJ = "##.###.###.####-##";
            MaskFormatter mf = new MaskFormatter(CNPJ);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(cnpj);
        } catch (ParseException ex) {
            return "";
        }
    }

    public Cnpj parse(String cnpj) throws ParseException {
        if ((cnpj != null) && (!cnpj.isEmpty())) {
            cnpj = cnpj.replaceAll("[^0-9]", "");
            if (isValid(cnpj)) {
                this.cnpj = cnpj;
                return this;
            } else {
                throw new ParseException(cnpj, 0);
            }
        } else {
            return null;
        }
    }

    public static boolean isValid(String cnpj) {
        final int[] FACTOR = {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
        final int LENGTH = 14;
        return isDefault(cnpj, LENGTH) && isValidDigit(cnpj, FACTOR, LENGTH);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.cnpj);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Cnpj other = (Cnpj) obj;

        return Objects.equals(this.cnpj, other.cnpj);
    }

    @Override
    public String toString() {
        return String.valueOf(cnpj);
    }

}
