package wallas.swing.table;

import java.util.List;
import javax.swing.table.TableModel;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <V>
 */
public interface JTableModel<V> extends TableModel {

    public abstract void insert(V value);

    public abstract void update(int rowIndex, V value);

    public abstract void update(V value);

    public abstract void delete(int rowIndex);

    public abstract void delete(V value);

    public abstract void deleteAll();

    public abstract void clear();

    public abstract V getElement(int rowIndex);

    public abstract List<V> getElements();

    public abstract List<V> getElementsChanged();

    public abstract void setElements(List<V> values);

}
