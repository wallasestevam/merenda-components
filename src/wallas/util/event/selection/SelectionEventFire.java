package wallas.util.event.selection;

import java.util.ArrayList;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class SelectionEventFire {

    protected final ArrayList<SelectionListener> listeners;

    public SelectionEventFire() {
        listeners = new ArrayList<>();
    }

    public void addListener(SelectionListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(SelectionListener listener) {
        listeners.remove(listener);
    }

    public void removeListeners() {
        listeners.clear();
    }

    public void fireValueSelected(Integer value) {
        SelectionEvent x = fireSelection(value);
        for (SelectionListener listener : listeners) {
            listener.valueSelected(x);
        }
    }

    public void fireNoneSelected(Integer value) {
        SelectionEvent x = fireSelection(value);
        for (SelectionListener listener : listeners) {
            listener.noneSelected(x);
        }
    }

    private SelectionEvent fireSelection(Object value) {
        return new SelectionEvent() {
            @Override
            public int getSelectionValue() {
                return getValue(value);
            }
        };
    }

    private Integer getValue(Object value) {
        if (value instanceof Integer) {
            return (Integer) value;
        } else {
            return null;
        }
    }

}
