package wallas.swing.textField.format;

import java.text.ParseException;
import javax.swing.text.Document;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.JTextFieldRefined.Format;
import wallas.swing.textField.JTextFieldRefined.FormatSee;
import wallas.util.form.cep.Cep;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CepFormatSee extends FormatSee {

    private static final String MASK = "  .   -   ";
    private JTextFieldRefined jTextFieldRefined;

    public CepFormatSee() {
    }

    @Override
    public void install(JTextFieldRefined jTextFieldRefined) {
        this.jTextFieldRefined = jTextFieldRefined;
        super.install(this.jTextFieldRefined);
    }

    @Override
    public Document getDocument() {
        return new JTextFieldRefined.MaskTextField(MASK, jTextFieldRefined);
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new Format() {
            @Override
            public Object stringToValue(String text) {
                return parse(text);
            }

            @Override
            public String valueToString(Object value) {
                Cep cep;
                if (value instanceof Cep) {
                    cep = (Cep) value;
                    return cep.format();
                } else if (value instanceof String) {
                    cep = parse((String) value);
                    return (cep != null) ? cep.format() : null;
                } else {
                    return null;
                }
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (text == null || text.equals(MASK)) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                if (parse(text) != null) {
                    setMessage("Cep Válido!");
                    return true;
                } else {
                    setMessage("Cep Inválido!");
                    return false;
                }
            }
        };
    }

    private Cep parse(String text) {
        try {
            Cep cep = new Cep();
            cep.parse(text);
            return cep;
        } catch (ParseException ex) {
            return null;
        }
    }

}
