package wallas.swing.textField.format;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import static javax.swing.SwingConstants.RIGHT;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import wallas.swing.textField.JTextFieldRefined;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class NumberFormatSee extends JTextFieldRefined.FormatSee {

    public static final String MONETARY = "R$ #,##0";
    public static final String NUMERIC = "#,##0";
    private JTextFieldRefined jTextFieldRefined;
    private NumberFormat numberFormat;
    private int maximumNumber;

    public NumberFormatSee() {
        this(-1, 2, null);
    }

    public NumberFormatSee(String format) {
        this(-1, 2, format);
    }

    public NumberFormatSee(int decimalPlaces, String format) {
        this(-1, decimalPlaces, format);
    }

    public NumberFormatSee(int maximumNumber, int decimalPlaces) {
        this(maximumNumber, decimalPlaces, null);
    }

    public NumberFormatSee(int maximumNumber, int decimalPlaces, String numberFormat) {
        setMaximumNumber(maximumNumber);
        setNumberFormat(decimalPlaces, numberFormat);
    }

    public int getMaximumNumber() {
        return maximumNumber;
    }

    public void setMaximumNumber(int maximumNumber) {
        this.maximumNumber = maximumNumber;
    }

    public NumberFormat getNumberFormat() {
        return numberFormat;
    }

    public void setNumberFormat(int decimalPlaces, String format) {
        format = (format == null) ? NUMERIC : format;
        this.numberFormat = new DecimalFormat((decimalPlaces == 0 ? format : format + ".") + makeZeros(decimalPlaces));
    }

    @Override
    public void install(JTextFieldRefined jTextFieldRefined) {
        this.jTextFieldRefined = jTextFieldRefined;

        super.install(this.jTextFieldRefined);

        this.jTextFieldRefined.setHorizontalAlignment(RIGHT);
        this.jTextFieldRefined.addCaretListener(caretListener());
        this.jTextFieldRefined.addKeyListener(keyAdapter());
        this.jTextFieldRefined.setText("0");
    }

    @Override
    public Document getDocument() {
        return new PlainDocument() {

            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                String text = new StringBuilder(jTextFieldRefined.getText().replaceAll("[^0-9]", "")).append(str.replaceAll("[^0-9]", "")).toString();

                super.remove(0, getLength());

                if (text.isEmpty()) {
                    text = "0";
                } else {
                    text = new BigInteger(text).toString();
                }

                if (getMaximumNumber() > 0 == text.length() > getMaximumNumber()) {
                    text = text.substring(0, getMaximumNumber());
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }

                super.insertString(0, numberFormat.format(bigDecimalFormat(text)), a);
            }

            @Override
            public void remove(int offs, int len) throws BadLocationException {
                super.remove(offs, len);
                if (len != getLength()) {
                    insertString(0, "", null);
                }
            }
        };
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new JTextFieldRefined.Format() {
            @Override
            public Object stringToValue(String text) {
                return bigDecimalFormat(text);
            }

            @Override
            public String valueToString(Object value) {
                return String.valueOf(value);
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (bigDecimalFormat(text).compareTo(BigDecimal.ZERO) == 0) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                return true;
            }
        };
    }

    private CaretListener caretListener() {
        return (CaretEvent e) -> {
            if (jTextFieldRefined.getText().length() != e.getDot()) {
                jTextFieldRefined.setCaretPosition(jTextFieldRefined.getText().length());
            }
        };
    }

    private KeyAdapter keyAdapter() {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    jTextFieldRefined.setText("");
                }
            }
        };
    }

    private static String makeZeros(int zeros) {
        if (zeros >= 0) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < zeros; i++) {
                builder.append('0');
            }
            return builder.toString();
        } else {
            throw new RuntimeException("Número de casas decimais inválida (" + zeros + ")");
        }
    }

    private BigDecimal bigDecimalFormat(String text) {
        return new BigDecimal(text.replaceAll("[^0-9]", "")).divide(new BigDecimal(Math.pow(10, numberFormat.getMaximumFractionDigits())));
    }

}
