package wallas.util.event;

/**
 *
 * @author José Wallas Clemente Estemva
 */
public interface GenericActionEvent {

    public Object getActionObject();

}
