package wallas.util.event;

import java.util.ArrayList;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class GenericEventFire {

    protected final ArrayList<GenericActionListener> listeners;

    public GenericEventFire() {
        listeners = new ArrayList<>();
    }

    public void addListener(GenericActionListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(GenericActionListener listener) {
        listeners.remove(listener);
    }

    public void removeListeners() {
        listeners.clear();
    }

    public void fireEvent(Object object) {
        GenericActionEvent x = new GenericActionEvent() {
            @Override
            public Object getActionObject() {
                return object;
            }
        };
        for (GenericActionListener listener : listeners) {
            listener.genericActionPerformed(x);
        }
    }

}
