package wallas.util.form.rfb;

import java.text.ParseException;
import java.util.Objects;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Cpf extends CpfCnpj {

    private String cpf;

    public Cpf() {
    }

    public String format() {
        try {
            String CPF = "###.###.###-##";
            MaskFormatter mf = new MaskFormatter(CPF);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(cpf);
        } catch (ParseException ex) {
            return "";
        }
    }

    public Cpf parse(String cpf) throws ParseException {
        if ((cpf != null) && (!cpf.isEmpty())) {
            cpf = cpf.replaceAll("[^0-9]", "");
            if (isValid(cpf)) {
                this.cpf = cpf;
                return this;
            } else {
                throw new ParseException(cpf, 0);
            }
        } else {
            return null;
        }
    }

    public static boolean isValid(String cpf) {
        final int[] FACTOR = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
        final int LENGTH = 11;
        return isDefault(cpf, LENGTH) && isValidDigit(cpf, FACTOR, LENGTH);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.cpf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final Cpf other = (Cpf) obj;

        return Objects.equals(this.cpf, other.cpf);
    }

    @Override
    public String toString() {
        return String.valueOf(cpf);
    }

}
