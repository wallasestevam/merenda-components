package wallas.swing.textField.format;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Locale;
import javax.swing.text.Document;
import wallas.swing.textField.JTextFieldRefined;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class DateFormatSee extends JTextFieldRefined.FormatSee {

    private static final String MASK = "  /  /    ";
    private JTextFieldRefined jTextFieldRefined;
    private final DateFormat dateFormat;

    public DateFormatSee() {
        dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("pt", "br"));
        dateFormat.setLenient(false);
    }

    @Override
    public void install(JTextFieldRefined jTextFieldRefined) {
        this.jTextFieldRefined = jTextFieldRefined;
        super.install(this.jTextFieldRefined);
    }

    @Override
    public Document getDocument() {
        return new JTextFieldRefined.MaskTextField(MASK, jTextFieldRefined);
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new JTextFieldRefined.Format() {
            @Override
            public Object stringToValue(String text) {
                return parse(text);
            }

            @Override
            public String valueToString(Object value) {
                return (value != null) ? dateFormat.format(value) : null;
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (text == null || text.equals(MASK)) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                if (parse(text) != null) {
                    setMessage("Data Válida!");
                    return true;
                } else {
                    setMessage("Data Inválida!");
                    return false;
                }
            }
        };
    }

    private Date parse(String text) {
        try {
            return new Date(dateFormat.parse(text).getTime());
        } catch (ParseException ex) {
            return null;
        }
    }

}
