package wallas.swing.dialog;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class JFrameToJDialog extends JDialog {

    private JFrameToJDialog() {
        super();
    }

    public JFrameToJDialog(JFrame frame) {
        this();
        add(frame.getContentPane());
        setModal(true);
        pack();
        setLocationRelativeTo(null);
        setTitle(frame.getTitle());
        setIconImage(frame.getIconImage());
    }

}
