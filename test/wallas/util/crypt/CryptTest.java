/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wallas.util.crypt;

import org.junit.Test;

/**
 *
 * @author Usuário
 */
public class CryptTest {

    @Test
    public void testEncryptAndDecrypt() throws Exception {
        Crypt crypt = new Crypt();
        String cpt = crypt.encrypt("wallas");
        System.out.println(cpt);
        String dct = crypt.decrypt(cpt);
        System.out.println(dct);
    }

}
