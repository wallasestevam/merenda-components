package wallas.swing.textField.format;

import static javax.swing.SwingConstants.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import wallas.swing.textField.JTextFieldRefined;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class IntegerFormatSee extends JTextFieldRefined.FormatSee {

    private int maxLen;

    public IntegerFormatSee() {
        this(-1);
    }

    public IntegerFormatSee(int maxLen) {
        setMaxLen(maxLen);
    }

    public int getMaxLen() {
        return maxLen;
    }

    public void setMaxLen(int maxLen) {
        this.maxLen = maxLen;
    }

    @Override
    public Document getDocument() {
        return new PlainDocument() {

            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (str.matches("^[0-9]*$") && (!(getMaxLen() > 0) == ((getLength() + 1) > getMaxLen()))) {
                    super.insertString(offs, str, a);
                } else {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
            }
        };
    }

    @Override
    public void install(JTextFieldRefined jTextFieldRefined) {
        super.install(jTextFieldRefined);
        jTextFieldRefined.setHorizontalAlignment(RIGHT);
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new JTextFieldRefined.Format() {
            @Override
            public Object stringToValue(String text) {
                return ((text == null) || text.trim().isEmpty()) ? null : Integer.parseInt(text);
            }

            @Override
            public String valueToString(Object value) {
                return ((value != null) && (value instanceof Integer)) ? String.valueOf(value) : null;
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (text == null || text.trim().isEmpty()) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                return true;
            }
        };
    }

}
