package wallas.swing.mask;

import java.text.ParseException;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class MaskObjects extends JFormattedTextField.AbstractFormatterFactory {

    public static final String MASK_CELULAR = "(##)#####-####";
    public static final String MASK_TELEFONE = "(##)####-####";
    public static final String MASK_CEP = "##.###-###";
    public static final String MASK_CNPJ = "##.###.###.####-##";
    public static final String MASK_CPF = "###.###.###-##";
    public static final String MASK_DATA = "##/##/####";
    private final String mask;

    public MaskObjects(String mask) {
        this.mask = mask;
    }

    @Override
    public JFormattedTextField.AbstractFormatter getFormatter(JFormattedTextField tf) {
        try {
            MaskFormatter maskFormatter = new MaskFormatter(mask);
            maskFormatter.setValueContainsLiteralCharacters(false);
            maskFormatter.setOverwriteMode(true);
            maskFormatter.setValidCharacters("0123456789");
            return maskFormatter;
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro ao Iniciar Máscara!", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
}
