package wallas.swing.textField.format;

import java.text.ParseException;
import javax.swing.text.Document;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.JTextFieldRefined.Format;
import wallas.swing.textField.JTextFieldRefined.FormatSee;
import wallas.util.form.phone.Cell;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CellFormatSee extends FormatSee {

    private static final String MASK = "(  )     -    ";
    private JTextFieldRefined jTextFieldRefined;

    public CellFormatSee() {
    }

    @Override
    public void install(JTextFieldRefined jTextFieldRefined) {
        this.jTextFieldRefined = jTextFieldRefined;
        super.install(this.jTextFieldRefined);
    }

    @Override
    public Document getDocument() {
        return new JTextFieldRefined.MaskTextField(MASK, jTextFieldRefined);
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new Format() {
            @Override
            public Object stringToValue(String text) {
                return parse(text);
            }

            @Override
            public String valueToString(Object value) {
                Cell cell;
                if (value instanceof Cell) {
                    cell = (Cell) value;
                    return cell.format();
                } else if (value instanceof String) {
                    cell = parse((String) value);
                    return (cell != null) ? cell.format() : null;
                } else {
                    return null;
                }
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (text == null || text.equals(MASK)) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                if (parse(text) != null) {
                    setMessage("Número Válido!");
                    return true;
                } else {
                    setMessage("Número Inválido!");
                    return false;
                }
            }
        };
    }

    private Cell parse(String text) {
        try {
            Cell cell = new Cell();
            cell.parse(text);
            return cell;
        } catch (ParseException ex) {
            return null;
        }
    }

}
