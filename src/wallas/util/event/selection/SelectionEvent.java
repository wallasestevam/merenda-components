package wallas.util.event.selection;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public interface SelectionEvent {

    public int getSelectionValue();

}
