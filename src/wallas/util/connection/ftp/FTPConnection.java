package wallas.util.connection.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPHTTPClient;
import org.apache.commons.net.ftp.FTPReply;
import wallas.util.event.progress.ProgressListener;
import wallas.util.fileCopyStream.FileCopyStream;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class FTPConnection {

    private FileCopyStream fcs;
    private FTPClient ftp;
    private FTPConfig ftpConfig;
    private FTPProxy ftpProxy;

    public FTPConnection() {
        this(null, null);
    }

    public FTPConnection(FTPConfig ftpConfig) {
        this(ftpConfig, null);
    }

    public FTPConnection(FTPConfig ftpConfig, FTPProxy ftpProxy) {
        this.ftpConfig = ftpConfig;
        this.ftpProxy = ftpProxy;
        this.fcs = new FileCopyStream();
    }

    public FTPConfig getFTPConfig() {
        return ftpConfig;
    }

    public void setFTPConfig(FTPConfig ftpConfig) {
        this.ftpConfig = ftpConfig;
    }

    public FTPProxy getFTPProxy() {
        return ftpProxy;
    }

    public void setFTPProxy(FTPProxy ftpProxy) {
        this.ftpProxy = ftpProxy;
    }

    public void connect() throws IOException {
        try {
            if (getFTPProxy() == null) {
                this.ftp = new FTPClient();
            } else {
                this.ftp = new FTPHTTPClient(getFTPProxy().getHost(), getFTPProxy().getPort(),
                        getFTPProxy().getUser(), getFTPProxy().getPassword());
            }

            ftp.connect(getFTPConfig().getHost());

            if (!FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
                ftp.disconnect();
                throw new IOException("A Conexão foi recusada. " + ftp.getReplyString());
            }

            if (!ftp.login(getFTPConfig().getUser(), getFTPConfig().getPassword())) {
                ftp.logout();
                throw new IOException("Não foi possível fazer o login. " + ftp.getReplyString());
            }

            if (!ftp.setFileType(FTP.BINARY_FILE_TYPE)) {
                throw new IOException("Não foi possível alterar para o modo de transferência binária. " + ftp.getReplyString());
            }
        } catch (IOException ex) {
            if (ftp.isConnected()) {
                ftp.disconnect();
            }
            throw new IOException("Não foi possível conectar ao servidor. " + ex.getMessage());
        }
    }

    public void disconnect() throws IOException {
        ftp.logout();
        ftp.disconnect();
    }

    public boolean isConnected() {
        return ftp.isConnected();
    }

    public void setActiveMode(boolean x) throws IOException {
        if (x) {
            ftp.enterLocalActiveMode();
        } else {
            ftp.enterLocalPassiveMode();
        }
    }

    public String[] fileNames(String directory) throws IOException {
        return ftp.listNames(directory);
    }

    public String[] fileNames() throws IOException {
        return ftp.listNames();
    }

    public boolean deleteFile(String pathName) throws IOException {
        return ftp.deleteFile(pathName);
    }

    public boolean changeDirectory(String directory) throws IOException {
        if (directory == null) {
            return ftp.changeToParentDirectory();
        } else {
            return ftp.changeWorkingDirectory(directory);
        }
    }

    public void download(String directory, File file) throws IOException {
        if (changeDirectory(directory)) {
            download(file);
        } else {
            throw new IOException("Não foi possivel localizar o diretório informado");
        }
    }

    public void download(File file) throws IOException {
        long length = ftp.mlistFile(file.getName()).getSize();

        fcs.copyStream(ftp.retrieveFileStream(file.getName()), new FileOutputStream(file), file.getCanonicalPath(), length);

        if (!ftp.completePendingCommand()) {
            throw new IOException("Não foi possível baixar o arquivo. " + ftp.getReplyString());
        }
    }

    public void upload(String directory, File file) throws IOException {
        if (changeDirectory(directory)) {
            upload(file);
        } else {
            throw new IOException("Não foi possivel localizar o diretório informado");
        }
    }

    public void upload(File file) throws IOException {
        fcs.copyStream(new FileInputStream(file), ftp.storeFileStream(file.getName()), file.getCanonicalPath(), file.length());

        if (!ftp.completePendingCommand()) {
            throw new IOException("Não foi possível subir o arquivo. " + ftp.getReplyString());
        }
    }

    public void addProgressListener(ProgressListener listener) {
        fcs.addProgressListener(listener);
    }

    public void removeProgressListener(ProgressListener listener) {
        fcs.removeProgressListener(listener);
    }

}
