package wallas.swing.combobox;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <V>
 */
public class JComboboxModel<V> extends AbstractListModel<V> implements ComboBoxModel<V> {

    private List<V> values;
    private V selected;
    private static final int FIRST = 0;

    public JComboboxModel() {
        this(new ArrayList<>());
    }

    public JComboboxModel(List<V> values) {
        this.values = values;
    }

    public List<V> getElements() {
        return values;
    }

    public void setElements(List<V> values) {
        this.values = values;
    }

    public void insert(V value) {
        getElements().add(value);
        int index = getSize() - 1;
        fireIntervalAdded(value, index, index);
    }

    public void delete(V value) {
        int index = getElements().indexOf(value);
        getElements().remove(index);
        fireIntervalRemoved(value, index, index);
    }

    public void clear() {
        getElements().clear();
    }

    @Override
    public V getElementAt(int index) {
        return getElements().get(index);
    }

    @Override
    public int getSize() {
        return getElements().size();
    }

    @Override
    public V getSelectedItem() {
        return selected;
    }

    @Override
    public void setSelectedItem(Object item) {
        if (item != null) {
            int index = getElements().indexOf(item);
            if (index != -1) {
                selected = getElementAt(index);
                fireContentsChanged(selected, index, index);
            }
        } else {
            setSelectedFirst();
        }
    }

    public void setSelectedFirst() {
        if (getSize() > 0) {
            selected = getElements().get(FIRST);
            fireContentsChanged(selected, FIRST, FIRST);
        }
    }

}
