package wallas.util.report;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import wallas.util.event.progress.ProgressEventFire;
import wallas.util.event.progress.ProgressListener;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PrintReportToPDF {

    private ProgressEventFire progressEvent;
    private String path;
    private String file;

    public PrintReportToPDF() {
    }

    public String getPath() {
        if (path == null) {
            return System.getProperty("java.io.tmpdir");
        }
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFile() {
        if (file == null) {
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy-HHmmss");
            GregorianCalendar calendar = new GregorianCalendar();
            return dateFormat.format(calendar.getTime());
        }
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    private String getAbsolutePath() {
        final String EXTENSION = ".pdf";
        return getPath() + getFile() + EXTENSION;
    }

    private void open(String absolutePath) throws IOException {
        final String COMMAND = "cmd.exe /C ";
        Runtime.getRuntime().exec(COMMAND + absolutePath);
    }

    public void print(String sql, String pathJasperTemplate, Connection dataBaseConnection) throws FileNotFoundException, SQLException, JRException, IOException {
        print(sql, new FileInputStream(pathJasperTemplate), dataBaseConnection);
    }

    public void print(String sql, InputStream jasperTemplate, Connection dataBaseConnection) throws JRException, IOException, SQLException {
        final String ABSOLUTE_PATH = getAbsolutePath();

        try (Connection con = dataBaseConnection; Statement stm = con.createStatement(); ResultSet rs = stm.executeQuery(sql)) {
            fireProgress("Conexão estabelecida", 10);
            JRDataSource jRResultSetDataSource = new JRResultSetDataSource(rs);
            fireProgress("Consulta de dados concluído", 30);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperTemplate, new HashMap(), jRResultSetDataSource);
            fireProgress("Criação do relatório concluído", 60);
            JasperExportManager.exportReportToPdfFile(jasperPrint, ABSOLUTE_PATH);
            fireProgress("Exportação do relatório completo", 100);
            open(ABSOLUTE_PATH);
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    protected void fireProgress(String status, int progress) {
        if (progressEvent != null) {
            progressEvent.fireNameProgressFile(status);
            progressEvent.fireProgressStatus(progress);
        }
    }

    public void addProgressListener(ProgressListener listener) {
        if (listener != null) {
            if (progressEvent == null) {
                progressEvent = new ProgressEventFire();
            }
            progressEvent.addListener(listener);
        }
    }

    public void removeProgressListener(ProgressListener listener) {
        if ((listener != null) && (progressEvent != null)) {
            progressEvent.removeListener(listener);
        }
    }

}
