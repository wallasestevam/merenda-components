package wallas.swing.textField.format;

import java.text.ParseException;
import javax.swing.text.Document;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.JTextFieldRefined.Format;
import wallas.util.form.rfb.Cpf;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CpfFormatSee extends JTextFieldRefined.FormatSee {

    private static final String MASK = "   .   .   -  ";
    private JTextFieldRefined jTextFieldRefined;

    public CpfFormatSee() {
    }

    @Override
    public void install(JTextFieldRefined jTextFieldRefined) {
        this.jTextFieldRefined = jTextFieldRefined;
        super.install(this.jTextFieldRefined);
    }

    @Override
    public Document getDocument() {
        return new JTextFieldRefined.MaskTextField(MASK, jTextFieldRefined);
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new Format() {
            @Override
            public Object stringToValue(String text) {
                return parse(text);
            }

            @Override
            public String valueToString(Object value) {
                Cpf cpf;
                if (value instanceof Cpf) {
                    cpf = (Cpf) value;
                    return cpf.format();
                } else if (value instanceof String) {
                    cpf = parse((String) value);
                    return (cpf != null) ? cpf.format() : null;
                } else {
                    return null;
                }
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (text == null || text.equals(MASK)) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                if (parse(text) != null) {
                    setMessage("CPF Válido!");
                    return true;
                } else {
                    setMessage("CPF Inválido!");
                    return false;
                }
            }
        };
    }

    private Cpf parse(String text) {
        try {
            Cpf cpf = new Cpf();
            cpf.parse(text);
            return cpf;
        } catch (ParseException ex) {
            return null;
        }
    }

}
