package wallas.swing.textField.format;

import java.text.ParseException;
import javax.swing.text.Document;
import wallas.swing.textField.JTextFieldRefined;
import wallas.swing.textField.JTextFieldRefined.Format;
import wallas.swing.textField.JTextFieldRefined.FormatSee;
import wallas.util.form.phone.Phone;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PhoneFormatSee extends FormatSee {

    private static final String MASK = "(  )    -    ";
    private JTextFieldRefined jTextFieldRefined;

    public PhoneFormatSee() {
    }

    @Override
    public void install(JTextFieldRefined jTextFieldRefined) {
        this.jTextFieldRefined = jTextFieldRefined;
        super.install(this.jTextFieldRefined);
    }

    @Override
    public Document getDocument() {
        return new JTextFieldRefined.MaskTextField(MASK, jTextFieldRefined);
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new Format() {
            @Override
            public Object stringToValue(String text) {
                return parse(text);
            }

            @Override
            public String valueToString(Object value) {
                Phone phone;
                if (value instanceof Phone) {
                    phone = (Phone) value;
                    return phone.format();
                } else if (value instanceof String) {
                    phone = parse((String) value);
                    return (phone != null) ? phone.format() : null;
                } else {
                    return null;
                }
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (text == null || text.equals(MASK)) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                if (parse(text) != null) {
                    setMessage("Número Válido!");
                    return true;
                } else {
                    setMessage("Número Inválido!");
                    return false;
                }
            }
        };
    }

    private Phone parse(String text) {
        try {
            Phone phone = new Phone();
            phone.parse(text);
            return phone;
        } catch (ParseException ex) {
            return null;
        }
    }

}
