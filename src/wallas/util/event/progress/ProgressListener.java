package wallas.util.event.progress;

/**
 *
 * @author José Wallas Clemente Estemva
 */
public interface ProgressListener {

    public void progressStatus(ProgressEvent evt);

    public void nameProgressFile(ProgressEvent evt);

}
