package wallas.util.form.rfb;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class CpfCnpj {

    private static int calculateDigit(String str, int[] factor) {
        int sum = 0;
        for (int index = str.length() - 1, digit; index >= 0; index--) {
            digit = Integer.parseInt(str.substring(index, index + 1));
            sum += digit * factor[factor.length - str.length() + index];
        }
        sum = 11 - sum % 11;
        return sum > 9 ? 0 : sum;
    }

    protected static boolean isValidDigit(String str, int[] factor, int length) {
        Integer digit1 = calculateDigit(str.substring(0, (length - 2)), factor);
        Integer digit2 = calculateDigit(str.substring(0, (length - 2)) + digit1, factor);
        return str.equals(str.substring(0, (length - 2)) + digit1.toString() + digit2.toString());
    }

    protected static boolean isDefault(String str, int length) {
        return str.matches("(?!(\\d)\\1{" + String.valueOf(length - 1) + "})\\d{" + String.valueOf(length) + "}");
    }

}
