package wallas.util.event;

/**
 *
 * @author José Wallas Clemente Estemva
 */
public interface GenericActionListener {

    public void genericActionPerformed(GenericActionEvent evt);

}
