package wallas.util.event.validator;

/**
 *
 * @author José Wallas Clemente Estemva
 */
public interface ValidatorActionListener {

    public void passed(ValidatorEvent evt);

    public void failed(ValidatorEvent evt);

    public void empty(ValidatorEvent evt);

}
