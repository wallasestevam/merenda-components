package wallas.swing.balloon;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.Timer;
import net.java.balloontip.BalloonTip;
import net.java.balloontip.styles.BalloonTipStyle;
import net.java.balloontip.styles.EdgedBalloonStyle;
import net.java.balloontip.styles.IsometricBalloonStyle;
import net.java.balloontip.styles.MinimalBalloonStyle;
import net.java.balloontip.styles.ModernBalloonStyle;
import net.java.balloontip.styles.RoundedBalloonStyle;
import net.java.balloontip.styles.ToolTipBalloonStyle;
import net.java.balloontip.utils.FadingUtils;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class BalloonMessage {

    public static enum Style {

        EDGED, ISOMETRIC, MINIMAL, MODERN, ROUNDED, TOOLTIP
    };
    private static final Color DEFAULT_FILL_COLOR = Color.WHITE;
    private static final Color DEFAULT_BORDER_COLOR = new Color(155, 155, 155);
    private final JComponent jComponent;
    private final Style style;
    private final boolean closeBalloon;
    private Color fillColor;
    private Color borderColor;
    private BalloonTip balloonTip;
    private String text;
    private boolean visible;
    private Timer timer;

    public BalloonMessage(JComponent jComponent, Style style, boolean closeBalloon) {
        this(jComponent, "", style, closeBalloon, DEFAULT_FILL_COLOR, DEFAULT_BORDER_COLOR);
    }

    public BalloonMessage(JComponent jComponent, String text, boolean closeBalloon) {
        this(jComponent, text, Style.TOOLTIP, closeBalloon, DEFAULT_FILL_COLOR, DEFAULT_BORDER_COLOR);
    }

    public BalloonMessage(JComponent jComponent, String text, Style style, boolean closeBalloon, Color fillColor, Color borderColor) {
        this.jComponent = jComponent;
        this.style = style;
        this.text = text;
        this.closeBalloon = closeBalloon;
        this.fillColor = fillColor;
        this.borderColor = borderColor;
        this.timer = new Timer(10000, actionPerformedTimer());
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        if (isVisible()) {
            balloonTip.setTextContents(getText());
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean result) {
        ActionListener onStop = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                balloonTip.setVisible(false);
                if (timer.isRunning()) {
                    timer.stop();
                }
            }
        };

        if (!(visible == result)) {
            if (result) {
                balloonTip = new BalloonTip(jComponent, new JLabel(getText()), setBalloonTipStyle(style), closeBalloon) {
                    @Override
                    public void closeBalloon() {
                        super.closeBalloon();
                        visible = false;
                    }
                };
                balloonTip.getContents().setForeground(Color.BLACK);
                balloonTip.setOpacity(0.0f);
                FadingUtils.fadeInBalloon(balloonTip, actionPerformedStartTimer(), 100, 24);
            } else {
                FadingUtils.fadeOutBalloon(balloonTip, onStop, 100, 24);
            }
            visible = result;
        }
    }

    private ActionListener actionPerformedTimer() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        };
    }

    private ActionListener actionPerformedStartTimer() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (timer.isRunning()) {
                    timer.restart();
                } else {
                    timer.start();
                }
            }
        };
    }

    private BalloonTipStyle setBalloonTipStyle(Style index) {
        switch (index) {
            case EDGED:
                return new EdgedBalloonStyle(getFillColor(), getBorderColor());

            case ISOMETRIC:
                return new IsometricBalloonStyle(getFillColor(), getBorderColor(), 5);

            case MINIMAL:
                Color transparentFill = new Color(getFillColor().getRed(), getFillColor().getGreen(), getFillColor().getBlue(), 180);
                return new MinimalBalloonStyle(transparentFill, 8);

            case MODERN:
                ModernBalloonStyle modernStyle = new ModernBalloonStyle(10, 10, getFillColor(), new Color(230, 230, 230), getBorderColor());
                modernStyle.setBorderThickness(3);
                modernStyle.enableAntiAliasing(true);
                return modernStyle;

            case ROUNDED:
                return new RoundedBalloonStyle(5, 5, getFillColor(), getBorderColor());

            default:
                return new ToolTipBalloonStyle(getFillColor(), getBorderColor());
        }
    }

}
