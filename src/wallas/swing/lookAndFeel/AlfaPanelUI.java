package wallas.swing.lookAndFeel;

import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.plaf.basic.BasicPanelUI;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class AlfaPanelUI extends BasicPanelUI {

    public AlfaPanelUI() {
    }

    @Override
    protected void installDefaults(JPanel p) {
        Color oldColor = p.getBackground();
        p.setBackground(null);

        LookAndFeel.installColorsAndFont(p,
                "Alfa.Panel.background",
                "Alfa.Panel.foreground",
                "Alfa.Panel.font");
        LookAndFeel.installBorder(p, "Alfa.Panel.border");
        LookAndFeel.installProperty(p, "opaque", Boolean.TRUE);

        if (p.getBackground() == null) {
            p.setBackground(oldColor);
        }
    }

}
