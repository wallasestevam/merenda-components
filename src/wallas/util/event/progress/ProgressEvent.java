package wallas.util.event.progress;

/**
 *
 * @author José Wallas Clemente Estemva
 */
public interface ProgressEvent {

    public int getProgress();

    public String getNameItem();

}
