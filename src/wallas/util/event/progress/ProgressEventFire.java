package wallas.util.event.progress;

import java.util.ArrayList;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class ProgressEventFire {

    protected final ArrayList<ProgressListener> listeners;

    public ProgressEventFire() {
        listeners = new ArrayList<>();
    }

    public void addListener(ProgressListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(ProgressListener listener) {
        listeners.remove(listener);
    }

    public void removeListeners() {
        listeners.clear();
    }

    public void fireProgressStatus(Integer progress) {
        ProgressEvent x = fireProgress(progress);
        for (ProgressListener listener : listeners) {
            listener.progressStatus(x);
        }
    }

    public void fireNameProgressFile(String nameFile) {
        ProgressEvent x = fireProgress(nameFile);
        for (ProgressListener listener : listeners) {
            listener.nameProgressFile(x);
        }
    }

    private ProgressEvent fireProgress(Object value) {
        return new ProgressEvent() {
            @Override
            public int getProgress() {
                return getValue(value);
            }

            @Override
            public String getNameItem() {
                return getName(value);
            }
        };

    }

    private String getName(Object value) {
        if (value instanceof String) {
            return (String) value;
        } else {
            return null;
        }
    }

    private Integer getValue(Object value) {
        if (value instanceof Integer) {
            return (Integer) value;
        } else {
            return null;
        }
    }

}
