package wallas.util.crypt;

import java.io.IOException;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author José Wallas Clemente Estemva
 */
public final class Crypt {

    private static final byte[] BYTES = new byte[]{3, 1, 4, 1, 5, 9, 2, 6};
    private static final String ALGORITHM = "PBEWithMD5AndDES";
    private static final String KEY = "4er68k55fgldk2nfms|dsdfere:fks@sfsser49%kdfa#jf29615!jdlf^$";
    private String algorithm;
    private String key;

    public Crypt() {
        this(null, null);
    }

    public Crypt(String key) {
        this(null, key);
    }

    public Crypt(String algorithm, String key) {
        setAlgorithm(algorithm);
        setKey(key);
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = (algorithm != null) ? algorithm : ALGORITHM;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = (key != null) ? key : KEY;
    }

    public final String encrypt(final String text) throws IOException {
        final Base64 enc;
        final Cipher cipher;
        try {
            enc = new Base64();
            cipher = Cipher.getInstance(getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, secretKey(getAlgorithm(), getKey()), parameterSpec(BYTES));
            return enc.encodeToString(cipher.doFinal(text.getBytes()));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException ex) {
            throw new IOException(ex);
        }
    }

    public final String decrypt(final String text) throws IOException {
        final Base64 dec;
        final Cipher cipher;
        try {
            dec = new Base64();
            cipher = Cipher.getInstance(getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, secretKey(getAlgorithm(), getKey()), parameterSpec(BYTES));
            return new String(cipher.doFinal(dec.decode(text)));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException ex) {
            throw new IOException(ex);
        }
    }

    private static PBEParameterSpec parameterSpec(byte[] bytes) {
        return new PBEParameterSpec(bytes, 20);
    }

    private static SecretKey secretKey(String algorithm, String cryptographicKey) throws InvalidKeySpecException, NoSuchAlgorithmException {
        SecretKeyFactory skf = SecretKeyFactory.getInstance(algorithm);
        KeySpec ks = new PBEKeySpec(cryptographicKey.toCharArray());
        return skf.generateSecret(ks);
    }

}
