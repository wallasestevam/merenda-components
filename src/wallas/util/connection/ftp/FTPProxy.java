package wallas.util.connection.ftp;

import java.io.Serializable;
import java.util.Objects;
import wallas.util.crypt.Crypt;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class FTPProxy implements Serializable {

    private String host;
    private int port;
    private String user;
    private String password;

    public FTPProxy() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = (port == null) ? 0 : port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isValid() {
        return (Objects.nonNull(host) && !host.trim().isEmpty())
                || (port != 0)
                || (Objects.nonNull(user) && !user.trim().isEmpty())
                || (Objects.nonNull(password) && !password.trim().isEmpty());
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 23 * hash + Objects.hashCode(this.host)
                + Objects.hashCode(this.port)
                + Objects.hashCode(this.user)
                + Objects.hashCode(this.password);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        final FTPProxy other = (FTPProxy) obj;

        return (Objects.equals(this.host, other.host))
                && (Objects.equals(this.port, other.port))
                && (Objects.equals(this.user, other.user))
                && (Objects.equals(this.password, other.password));
    }

    private void writeObject(java.io.ObjectOutputStream stream) throws java.io.IOException {
        final Crypt crypt = new Crypt();

        setHost(crypt.encrypt(getHost()));
        setPort(Integer.parseInt(crypt.encrypt(String.valueOf(getPort()))));
        setUser(crypt.encrypt(getUser()));
        setPassword(crypt.encrypt(getPassword()));

        stream.defaultWriteObject();
    }

    private void readObject(java.io.ObjectInputStream stream) throws java.io.IOException, ClassNotFoundException {
        final Crypt crypt = new Crypt();

        stream.defaultReadObject();

        setHost(crypt.decrypt(getHost()));
        setPort(Integer.parseInt(crypt.decrypt(String.valueOf(getPort()))));
        setUser(crypt.decrypt(getUser()));
        setPassword(crypt.decrypt(getPassword()));
    }

}
