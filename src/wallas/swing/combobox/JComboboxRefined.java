package wallas.swing.combobox;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;
import wallas.swing.textField.JTextFieldRefined;

/**
 *
 * @author José Wallas Clemente Estevam
 * @param <V>
 */
public class JComboboxRefined<V> extends JComboBox<V> {

    private Display display;

    public JComboboxRefined() {
        this(null, false);
    }

    public JComboboxRefined(boolean required) {
        this(null, required);
    }

    public JComboboxRefined(Display display, boolean required) {
        super();

        setDisplay(display);
        setEditor(new DefaultComboBoxEditor(required));
        initAutoComplete();
    }

    public Display getDisplay() {
        return display;
    }

    public void setDisplay(Display display) {
        if ((display != null)) {
            this.display = display;
        }
    }

    @Override
    public void setEditable(boolean x) {
        if (!isEditable()) {
            super.setEditable(x);
        }
    }

    public JTextFieldRefined getEditorComponent() {
        return (JTextFieldRefined) getEditor().getEditorComponent();
    }

    private void initAutoComplete() {
        if (getDisplay() == null) {
            AutoCompleteDecorator.decorate(this, ObjectToStringConverter.DEFAULT_IMPLEMENTATION);
        } else {
            AutoCompleteDecorator.decorate(this, getDisplay());
            setRenderer(new DefaultComboBoxRenderer(getDisplay()));
        }
    }

    private static final class DefaultComboBoxEditor implements ComboBoxEditor, FocusListener {

        private final JTextFieldRefined editor;

        public DefaultComboBoxEditor(boolean required) {
            editor = createEditorComponent(required);
        }

        @Override
        public Component getEditorComponent() {
            return editor;
        }

        protected JTextFieldRefined createEditorComponent(boolean required) {
            JTextFieldRefined newEditor = new BorderlessTextField(required);
            newEditor.setBorder(null);
            return newEditor;
        }

        @Override
        public void setItem(Object anObject) {
            String text;

            if (anObject != null) {
                text = anObject.toString();
                if (text == null) {
                    text = "";
                }
            } else {
                text = "";
            }

            if (!text.equals(editor.getValue())) {
                editor.setValue(text);
            }
        }

        @Override
        public Object getItem() {
            return editor.getValue();
        }

        @Override
        public void selectAll() {
            editor.selectAll();
            editor.requestFocus();
        }

        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
        }

        @Override
        public void addActionListener(ActionListener l) {
            editor.addActionListener(l);
        }

        @Override
        public void removeActionListener(ActionListener l) {
            editor.removeActionListener(l);
        }

        static class BorderlessTextField extends JTextFieldRefined {

            public BorderlessTextField(boolean required) {
                super(required);
            }

            @Override
            public void setValue(Object s) {
                if (getValue().equals(s)) {
                    return;
                }
                super.setValue(s);
            }

            @Override
            public void setBorder(Border b) {
                if (!(b instanceof UIResource)) {
                    super.setBorder(b);
                }
            }
        }

        public static class UIResource extends BasicComboBoxEditor
                implements javax.swing.plaf.UIResource {
        }

    }

    private static final class DefaultComboBoxRenderer extends BasicComboBoxRenderer {

        private final Display displayValue;

        public DefaultComboBoxRenderer(Display displayValue) {
            this.displayValue = displayValue;
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object item, int index, boolean isSelected, boolean hasFocus) {
            super.getListCellRendererComponent(list, item, index, isSelected, hasFocus);
            if (item != null) {
                setText(displayValue.getPreferredStringForItem(item));
            }
            return this;
        }

    }

    public static class Display extends ObjectToStringConverter {

        @Override
        public String getPreferredStringForItem(Object value) {
            return String.valueOf(value);
        }

    }

}
