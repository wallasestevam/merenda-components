package wallas.swing.textField;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import wallas.swing.balloon.BalloonMessage;
import wallas.swing.textField.format.StringFormatSee;
import wallas.util.event.validator.ValidatorActionListener;
import wallas.util.event.validator.ValidatorEvent;
import wallas.util.event.validator.ValidatorEventFire;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class JTextFieldRefined extends JTextField {

    private BalloonMessage balloon;
    private FormatSee formatSee;
    private Format format;
    private boolean required;
    private boolean validData;
    private final JTextField jTextField;

    public JTextFieldRefined() {
        this(null, false, null);
    }

    public JTextFieldRefined(boolean required) {
        this(null, required, null);
    }

    public JTextFieldRefined(boolean required, BalloonMessage balloon) {
        this(null, required, balloon);
    }

    public JTextFieldRefined(FormatSee formatSee, boolean required) {
        this(formatSee, required, null);
    }

    public JTextFieldRefined(FormatSee formatSee, boolean required, BalloonMessage balloon) {
        super();

        this.jTextField = new JTextField();

        setFormatSee(formatSee);
        setRequired(required);
        setBalloon(balloon);
        addFocusListener(getFocusListener());
    }

    public BalloonMessage getBalloon() {
        return balloon;
    }

    public void setBalloon(BalloonMessage balloon) {
        if (balloon == null) {
            this.balloon = new BalloonMessage(this, BalloonMessage.Style.MODERN, true);
        } else {
            this.balloon = balloon;
        }
    }

    public FormatSee getFormatSee() {
        return formatSee;
    }

    public void setFormatSee(FormatSee formatSee) {
        if (formatSee == null) {
            this.formatSee = new StringFormatSee();
        } else {
            this.formatSee = formatSee;
        }
        this.formatSee.install(this);

        getValidator().addValidatorListener(getValidatorActionListener());
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public Validator getValidator() {
        return (getInputVerifier() instanceof Validator) ? (Validator) getInputVerifier() : null;
    }

    public void setValidator(Validator validator) {
        setInputVerifier(validator);
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isValidData() {
        return validData;
    }

    public boolean isValidDataShow() {
        if (isEnabled() && !isValidData()) {
            setBackground(Color.PINK);
        } else {
            setBackground(jTextField.getBackground());
        }
        return isValidData();
    }

    public void validData() {
        validData = getInputVerifier().verify(this);
    }

    public Object getValue() {
        return getFormat().stringToValue(getText());
    }

    public void setValue(Object value) {
        setText(getFormat().valueToString(value));
        validData();
    }

    @Override
    public void setEnabled(boolean x) {
        super.setEnabled(x);
        jTextField.setEnabled(x);

        if (x) {
            validData();
        } else {
            showStateStandard();
        }
    }

    @Override
    public void setDocument(Document doc) {
        super.setDocument(doc);

        if (doc instanceof MaskTextField) {
            setText(((MaskTextField) doc).getMask());
        }
    }

    private ValidatorActionListener getValidatorActionListener() {
        return new ValidatorActionListener() {
            @Override
            public void passed(ValidatorEvent evt) {
                showStateStandard();
            }

            @Override
            public void failed(ValidatorEvent evt) {
                showWhetherInvalidData();
            }

            @Override
            public void empty(ValidatorEvent evt) {
                showWhetherInvalidData();
            }
        };
    }

    private FocusListener getFocusListener() {
        return new FocusAdapter() {

            @Override
            public void focusGained(FocusEvent e) {
                visibleBalloonToClose();
            }
        };
    }

    private void showStateStandard() {
        visibleBalloonToClose();
        isValidDataShow();
    }

    private void showWhetherInvalidData() {
        getBalloon().setText(getValidator().getMessage());
        getBalloon().setVisible(true);
        isValidDataShow();
    }

    private void visibleBalloonToClose() {
        if (getBalloon().isVisible()) {
            getBalloon().setVisible(false);
        }
    }

    public static abstract class FormatSee {

        public void install(JTextFieldRefined jTextFieldRefined) {
            jTextFieldRefined.setValidator(getValidator());
            jTextFieldRefined.setFormat(getFormat());
            jTextFieldRefined.setDocument(getDocument());
        }

        public abstract Validator getValidator();

        public abstract Format getFormat();

        public abstract Document getDocument();

    }

    public static abstract class Format {

        public abstract Object stringToValue(String text);

        public abstract String valueToString(Object value);

    }

    public static abstract class Validator extends InputVerifier {

        private final ValidatorEventFire validatorEvent;
        private String message;

        public Validator() {
            validatorEvent = new ValidatorEventFire();
            message = "";
        }

        public String getMessage() {
            return message;
        }

        protected void setMessage(String message) {
            this.message = message;
        }

        @Override
        public boolean shouldYieldFocus(JComponent input) {
            JTextFieldRefined jTextFieldRefined = getJTextFieldRefined(input);
            jTextFieldRefined.validData();

            if (isEmpty(jTextFieldRefined.getText())) {
                if (jTextFieldRefined.isRequired()) {
                    validatorEvent.fireEventEmpty(null);
                }
                return true;
            } else if (isValid(jTextFieldRefined.getText())) {
                validatorEvent.fireEventPassed(null);
                return true;
            } else {
                validatorEvent.fireEventFailed(null);
                return false;
            }
        }

        @Override
        public boolean verify(JComponent input) {
            JTextFieldRefined jTextFieldRefined = getJTextFieldRefined(input);

            if (jTextFieldRefined.isRequired()) {
                return !isEmpty(jTextFieldRefined.getText()) && isValid(jTextFieldRefined.getText());
            } else {
                return isEmpty(jTextFieldRefined.getText()) || isValid(jTextFieldRefined.getText());
            }
        }

        private JTextFieldRefined getJTextFieldRefined(JComponent input) {
            if (input instanceof JTextFieldRefined) {
                return (JTextFieldRefined) input;
            } else {
                return null;
            }
        }

        public void addValidatorListener(ValidatorActionListener listener) {
            validatorEvent.addListener(listener);
        }

        public void removeValidatorListener(ValidatorActionListener listener) {
            validatorEvent.removeListener(listener);
        }

        public abstract boolean isEmpty(String text);

        public abstract boolean isValid(String text);

    }

    public static class MaskTextField extends PlainDocument {

        private final String mask;
        private final JTextFieldRefined jTextFieldRefined;
        private final ArrayList<Integer> specialPosition;
        private StringBuffer newText;
        private boolean keyDelete;

        public MaskTextField(String mask, JTextFieldRefined jTextFieldRefined) {
            this.mask = mask;
            this.jTextFieldRefined = jTextFieldRefined;
            this.specialPosition = createListSpecialPosition();
            this.jTextFieldRefined.addKeyListener(getKeyAdapter());
        }

        public String getMask() {
            return mask;
        }

        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if (jTextFieldRefined.getValidator().isValid(str) || str.equals(getMask())) {
                replaceAll(str, a);
            } else if (isValidCharacters(str) && (offs < getMask().length())) {
                replaceRight(offs, str.charAt(0), a);
            } else {
                java.awt.Toolkit.getDefaultToolkit().beep();
            }
        }

        @Override
        public void remove(int offs, int len) throws BadLocationException {
            if (len == getLength()) {
                replaceAll(getMask(), null);
            } else if (offs < getMask().length()) {
                if (keyDelete) {
                    replaceRight(offs, ' ', null);
                } else {
                    replaceLeft(offs, ' ', null);
                }
            }
        }

        private void replaceRight(int offs, char replacement, AttributeSet a) throws BadLocationException {
            if (isSpecialPosition(offs)) {
                replace(offs + 1, replacement, a);
                jTextFieldRefined.setCaretPosition(offs + 2);
            } else {
                replace(offs, replacement, a);
                jTextFieldRefined.setCaretPosition(offs + 1);
            }
        }

        private void replaceLeft(int offs, char replacement, AttributeSet a) throws BadLocationException {
            if (isSpecialPosition(offs)) {
                replace(offs - 1, replacement, a);
                jTextFieldRefined.setCaretPosition(offs - 1);
            } else {
                replace(offs, replacement, a);
                jTextFieldRefined.setCaretPosition(offs);
            }
        }

        private void replace(int offs, char replacement, AttributeSet a) throws BadLocationException {
            newText.setCharAt(offs, replacement);
            super.remove(0, getLength());
            super.insertString(0, newText.toString(), a);
        }

        private void replaceAll(String replacement, AttributeSet a) throws BadLocationException {
            newText = new StringBuffer(replacement);
            super.remove(0, getLength());
            super.insertString(0, newText.toString(), a);
        }

        private boolean isValidCharacters(String text) {
            ArrayList<Boolean> arrayList = new ArrayList<>();
            for (int i = 0; i < text.length(); i++) {
                arrayList.add(Character.isDigit(text.charAt(i)));
            }
            return !arrayList.contains(false);
        }

        private boolean isSpecialPosition(int offs) {
            return specialPosition.contains(offs);
        }

        private ArrayList<Integer> createListSpecialPosition() {
            ArrayList<Integer> sp = new ArrayList<>();
            for (int i = 0; i < getMask().length(); i++) {
                if (getMask().charAt(i) != ' ') {
                    sp.add(i);
                }
            }
            return sp;
        }

        private KeyAdapter getKeyAdapter() {
            return new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    keyDelete = e.getKeyCode() == KeyEvent.VK_DELETE;
                }
            };
        }

    }

}
