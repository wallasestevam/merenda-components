package wallas.util.event.selection;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public interface SelectionListener {

    public void valueSelected(SelectionEvent evt);

    public void noneSelected(SelectionEvent evt);

}
