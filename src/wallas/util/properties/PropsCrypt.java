package wallas.util.properties;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Properties;
import wallas.util.crypt.Crypt;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class PropsCrypt extends Properties {

    private final Crypt crypt = new Crypt("kfjsdfl;qejori13458&*(&%^&^%^#$TGYT^&%^&TGG+)(=0=");

    public PropsCrypt() {
        super();
    }

    public PropsCrypt(Properties defaults) {
        super(defaults);
    }

    @Override
    public synchronized void load(InputStream inStream) throws IOException {
        load(new InputStreamReader(inStream));
    }

    @Override
    public synchronized void load(Reader reader) throws IOException {
        Properties prop = new Properties();
        prop.load(reader);
        prop.entrySet().forEach(values -> {
            try {
                put(crypt.decrypt((String) values.getKey()),
                        crypt.decrypt((String) values.getValue()));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    @Override
    public void store(OutputStream out, String comments) throws IOException {
        store(new OutputStreamWriter(out), comments);
    }

    @Override
    public void store(Writer writer, String comments) throws IOException {
        Properties prop = new Properties();

        entrySet().forEach(values -> {
            try {
                prop.put(crypt.encrypt(String.valueOf(values.getKey())),
                        crypt.encrypt(String.valueOf(values.getValue())));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        prop.store(writer, comments);
    }

}
