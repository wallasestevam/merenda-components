package wallas.swing.textField.format;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import wallas.swing.textField.JTextFieldRefined;

/**
 *
 * @author Usuário
 */
public class StringFormatSee extends JTextFieldRefined.FormatSee {

    private int minLen;
    private int maxLen;

    public StringFormatSee() {
        this(-1, -1);
    }

    public StringFormatSee(int maxLen) {
        this(-1, maxLen);
    }

    public StringFormatSee(int minLen, int maxLen) {
        setMinLen(minLen);
        setMaxLen(maxLen);
    }

    public int getMinLen() {
        return minLen;
    }

    public void setMinLen(int minLen) {
        this.minLen = minLen;
    }

    public int getMaxLen() {
        return maxLen;
    }

    public void setMaxLen(int maxLen) {
        this.maxLen = maxLen;
    }

    @Override
    public Document getDocument() {
        return new PlainDocument() {

            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (!(getMaxLen() > 0) == ((getLength() + 1) > getMaxLen())) {
                    super.insertString(offs, str, a);
                } else {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
            }
        };
    }

    @Override
    public JTextFieldRefined.Format getFormat() {
        return new JTextFieldRefined.Format() {
            @Override
            public Object stringToValue(String text) {
                return text;
            }

            @Override
            public String valueToString(Object value) {
                return (value != null) ? String.valueOf(value) : null;
            }
        };
    }

    @Override
    public JTextFieldRefined.Validator getValidator() {
        return new JTextFieldRefined.Validator() {
            @Override
            public boolean isEmpty(String text) {
                if (text == null || text.trim().isEmpty()) {
                    setMessage("Campo Obrigatório!");
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isValid(String text) {
                if (getMinLen() > 0) {
                    if (text.length() < getMinLen()) {
                        setMessage("O campo deve ter no mínimo " + getMinLen() + " caracteres");
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        };
    }

}
